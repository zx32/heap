#include <stdio.h>  // needed for size_t
#include <unistd.h> // needed for sbrk
#include <assert.h> // needed for asserts
#include "dmm.h"

/* You can improve the below metadata structure using the concepts from Bryant
 * and OHallaron book (chapter 9).
 */

typedef struct metadata {
  /* size_t is the return type of the sizeof operator. Since the size of an
   * object depends on the architecture and its implementation, size_t is used
   * to represent the maximum size of any object in the particular
   * implementation. size contains the size of the data object or the number of
   * free bytes
   */
  size_t size;
  struct metadata* next;
  struct metadata* prev; 
  bool isAllocated;
} metadata_t;

/* freelist maintains all the blocks which are not in use; freelist is kept
 * sorted to improve coalescing efficiency 
 */

static metadata_t* freelist = NULL;

void* dmalloc(size_t numbytes) {
  /* initialize through sbrk call first time */
  if(freelist == NULL) {      
    if(!dmalloc_init())
      return NULL;
  }


  assert(numbytes > 0);

  /* your code here */
  metadata_t* curblock = freelist;


  while (curblock != NULL){
    //printf("%s\n", "stuck");

    //if it is the same size and free, no need to create the next block, take the whole block
    //only need to update the is Allocated and return the current block, no need to change the prev next size
    if ((ALIGN(numbytes) == curblock->size) && (curblock->isAllocated == false)){
      curblock->isAllocated = true;
      return (((void*) curblock) + METADATA_T_ALIGNED);
    }

    if (((ALIGN(numbytes) + METADATA_T_ALIGNED) < curblock->size) && (curblock->isAllocated == false)){
      //if it is smaller and free
      //create the new block after curblock

      metadata_t* nextblock = (metadata_t*) (((void*) curblock) + ALIGN(numbytes) + METADATA_T_ALIGNED);
      nextblock->size = curblock->size - METADATA_T_ALIGNED-ALIGN(numbytes);
      nextblock->prev = curblock;
      nextblock->next = curblock->next;
      nextblock->isAllocated = false;

      if (curblock->next != NULL){
        curblock->next->prev = nextblock;
      }

      //modify curblock
      curblock->size = ALIGN(numbytes);
      curblock->next = nextblock;
      curblock->isAllocated = true;
      //return address is curblock + header
      return (((void*) curblock) + METADATA_T_ALIGNED);
    }
    curblock = curblock->next;
    
  }


  return NULL;
}

void dfree(void* ptr) {
  /* your code here */
  //ptr is the address of the block freed, -heap to get to deallocate to get the metablock
  metadata_t* freeblock = (metadata_t*) (ptr - METADATA_T_ALIGNED);
  if (freeblock == NULL) {
    return;
  }
  //check prev and next block to see if should coalesce
  //it has a prev block and is not allocated
  if ((freeblock->prev != NULL)&&(freeblock->prev->isAllocated == false)){
    //update blocksize + heapsize
    //update prev pointer of freeblock to prev of prev
    //update prev prev's next pointer
    //printf("\n%s\t%d\t%p\n\n", "LEFT!!!!!!!!!!!",freeblock->prev->isAllocated,freeblock->prev);
    freeblock->prev->size = freeblock->size + freeblock->prev->size + METADATA_T_ALIGNED;
    freeblock->prev->next = freeblock->next;
    if (freeblock->next != NULL){
      freeblock->next->prev = freeblock->prev;
    } 
    freeblock = freeblock->prev;
  }

  //check next block too
  if ((freeblock->next != NULL) && (freeblock->next->isAllocated == false)){
    //printf("\n%s\n\n", "RIGHT!!!!!!!!!!!");
    freeblock->size = freeblock->size + freeblock->next->size + METADATA_T_ALIGNED;
    freeblock->next = freeblock->next->next;

    if (freeblock->next != NULL){
      freeblock->next->prev = freeblock;
    }
  }

  freeblock->isAllocated = false;

}

bool dmalloc_init() {

  /* Two choices: 
   * 1. Append prologue and epilogue blocks to the start and the
   * end of the freelist 
   *
   * 2. Initialize freelist pointers to NULL
   *
   * Note: We provide the code for 2. Using 1 will help you to tackle the 
   * corner cases succinctly.
   */

  size_t max_bytes = ALIGN(MAX_HEAP_SIZE);
  /* returns heap_region, which is initialized to freelist */
  freelist = (metadata_t*) sbrk(max_bytes); 
  /* Q: Why casting is used? i.e., why (void*)-1? */
  if (freelist == (void *)-1)
    return false;
  freelist->next = NULL;
  freelist->prev = NULL;
  freelist->size = max_bytes-METADATA_T_ALIGNED;
  freelist->isAllocated = false;
  return true;
}

/* for debugging; can be turned off through -NDEBUG flag*/
void print_freelist() {
  metadata_t *freelist_head = freelist;
  while(freelist_head != NULL) {
    printf("\tFreelist Size:%zd, Head:%p, Prev:%p, Next:%p, Allocated:%p\n",
    freelist_head->size,
    freelist_head,
    freelist_head->prev,
    freelist_head->next,
    freelist_head->isAllocated);

    freelist_head = freelist_head->next;
  }
  printf("\n");
}
